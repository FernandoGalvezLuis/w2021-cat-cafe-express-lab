import express from "express";
import cats from "./cats.js";

const router = express.Router();

//TODO1:
router.get("/", (req, res) => res.send("This is the landing page"));

//TODO2:
router.get("/cats", (req, res) => {
  return res.status(200).send(cats);
});

//TODO3:
router.post("/cats", (req, res) => {
  const { id, name, breed } = req.body;
  if (!name || !breed || !id) {
    return res.status(400).send("Error, form fields are missing information");
  } else {
    cats.push(req.body);
    return res.status(201).send(cats);
  }
});

//TODO4:
router.delete("/cats", (req, res) => {
  let index = cats.findIndex((cat) => cat.id === req.body.id);
  cats.splice(index, 1);
  return res.status(200).send(cats);
});

//TODO5:
router.get("/cats/:id", (req, res) => {
  let index = cats.findIndex((cat) => cat.id == req.params.id);
  return res.status(200).send(cats[index]);
});

//TODO6:
router.put("/cats/:id", (req, res) => {
  let catObject = cats.find((catObj) => catObj.id == req.params.id);
  if (catObject == null || catObject == undefined) {
    return res.status(404).send("Cannot find cat");
  } else if(req.body.id != req.params.id) {
    return res.status(400).send("Bad request");
  } else {
    let index = cats.findIndex((cat) => cat.id == req.params.id);
    cats.splice(index, 1, req.body);
    return res.status(200).send(cats[index]);
  }
});

export default router;
